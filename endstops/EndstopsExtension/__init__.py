from labthings import fields, find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.views import ActionView, PropertyView, View
from openflexure_microscope.api.utilities.gui import build_gui


class HomeAxesActionView(ActionView):
    args = {
        "direction": fields.String(required=True),
        "axis": fields.String(required=True),
        "state": fields.String(
            required=False, allow_none=True
        ),  # ignored but required for the form
    }

    def post(self, args):
        extension = find_extension("org.openflexure.endstops")
        return extension.home(args["direction"], args["axis"])


class EndstopStatus(View):
    def get(self):
        extension = find_extension("org.openflexure.endstops")

        return extension.endstop_state()


class MaxPositionsView(PropertyView):
    schema = {
        "max_x": fields.Integer(required=True),
        "max_y": fields.Integer(required=True),
        "max_z": fields.Integer(required=True),
    }

    def get(self):
        extension = find_extension("org.openflexure.endstops")

        maxima = extension.get_max_positions()
        return {"max_x": maxima[0], "max_y": maxima[1], "max_z": maxima[2]}

    def post(self, args):
        extension = find_extension("org.openflexure.endstops")
        extension.set_max_positions(
            args.get("max_x"), args.get("max_y"), args.get("max_z")
        )

        maxima = extension.get_max_positions()
        return {"max_x": maxima[0], "max_y": maxima[1], "max_z": maxima[2]}


# Create the extension class
class EndstopsExtension(BaseExtension):
    def __init__(self):
        super().__init__("org.openflexure.endstops", version="0.0.1")

        def gui_func():
            endstop_status = self.endstop_state()
            html = "<table><tr><th>X</th><th>Y</th><th>Z</th></tr><tr>"
            for state in endstop_status:
                html += f"<td>{state}</td>"
            html += "</tr></table>"

            installed_endstops = self.installed_endstops()

            if installed_endstops:
                forms = [
                    {
                        "name": "Home",
                        "route": "/home-stage",
                        "isTask": True,
                        "isCollapsible": True,
                        "submitLabel": "Home",
                        "schema": [
                            {
                                "fieldType": "selectList",
                                "name": "direction",
                                "label": "Direction",
                                "value": "min",
                                "options": [
                                    direction
                                    for direction in installed_endstops
                                    if direction != "soft"
                                ],
                            },
                            {
                                "fieldType": "selectList",
                                "name": "axis",
                                "label": "Axis",
                                "value": "x",
                                "options": ["x", "y", "z"],
                            },
                            {
                                "fieldType": "htmlBlock",
                                "name": "state",
                                "label": "State",
                                "content": html,
                            },
                        ],
                    }
                ]

                if "max" in installed_endstops or "soft" in installed_endstops:
                    maxima = self.get_max_positions()
                    forms.append(
                        {
                            "name": "Set maximum",
                            "route": "/max-positions",
                            "isTask": False,
                            "isCollapsible": True,
                            "submitLabel": "Set",
                            "schema": [
                                {
                                    "fieldType": "numberInput",
                                    "name": "max_x",
                                    "label": "Max X",
                                    "value": maxima[0],
                                },
                                {
                                    "fieldType": "numberInput",
                                    "name": "max_y",
                                    "label": "Max Y",
                                    "value": maxima[1],
                                },
                                {
                                    "fieldType": "numberInput",
                                    "name": "max_z",
                                    "label": "Max Z",
                                    "value": maxima[2],
                                },
                            ],
                        }
                    )
            else:
                forms = []

            return {"icon": "border_bottom", "forms": forms}

        self.add_view(EndstopStatus, "/endstop-status")
        self.add_view(HomeAxesActionView, "/home-stage")
        self.add_view(MaxPositionsView, "/max-positions")

        self.add_meta("gui", build_gui(gui_func, self))

    def installed_endstops(self):
        microscope = find_component("org.openflexure.microscope")
        sangastage = microscope.stage.board
        if not hasattr(sangastage, "endstops"):
            return []

        return sangastage.endstops.installed

    def endstop_state(self):
        microscope = find_component("org.openflexure.microscope")
        sangastage = microscope.stage.board

        if not hasattr(sangastage, "endstops"):
            return "Not supported"
        return sangastage.endstops.status

    def get_max_positions(self):
        microscope = find_component("org.openflexure.microscope")
        sangastage = microscope.stage.board

        if not hasattr(sangastage, "endstops"):
            raise NotImplementedError("Endstops not installed")

        return sangastage.endstops.maxima

    def set_max_positions(self, max_x, max_y, max_z):
        microscope = find_component("org.openflexure.microscope")
        sangastage = microscope.stage.board

        if not hasattr(sangastage, "endstops"):
            raise NotImplementedError("Endstops not installed")

        sangastage.endstops.maxima = (max_x, max_y, max_z)

    def home(self, direction, axes):
        microscope = find_component("org.openflexure.microscope")
        sangastage = microscope.stage.board

        if not hasattr(sangastage, "endstops"):
            return "Not supported"

        if len(axes) > 1:
            return "Homing multiple axes is currently not supported due to mechanical limitations"
        sangastage.endstops.home(direction, list(axes))
        sangastage.move_rel(25000, axes)
        sangastage.move_rel(25000, axes)
        # multiple axes at 0 are out of motion range on the Delta stage due to low o-ring tension
