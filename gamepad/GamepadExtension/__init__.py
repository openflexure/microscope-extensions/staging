from labthings.extensions import BaseExtension
from labthings.utilities import path_relative_to
from openflexure_microscope.api.utilities.gui import build_gui

# Create the extension class
class GamepadExtension(BaseExtension):
    def __init__(self):
        super().__init__(
            "org.openflexure.gamepad",
            version="0.0.1",
            static_folder=path_relative_to(__file__, "static"),
        )

        def gui_func():
            return {"icon": "sports_esports", "frame": {"href": self.static_file_url("")}}

        self.add_meta("gui", build_gui(gui_func, self))