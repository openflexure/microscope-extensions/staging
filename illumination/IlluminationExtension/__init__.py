from labthings import current_action, fields, find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.views import ActionView, PropertyView, View
from openflexure_microscope.api.utilities.gui import build_gui
from sangaboard import Sangaboard

class SetIlluminationView(ActionView):
    args = {
        "pwm1": fields.Int(required=True),
        "pwm2": fields.Int(required=True),
        "cc": fields.Int(required=True),
    }

    def post(self, args):
        extension = find_extension("org.openflexure.illumination")
        action = current_action()

        return extension.set(action, args["pwm1"], args["pwm2"], args["cc"])

# Create the extension class
class IlluminationExtension(BaseExtension):
    def __init__(self):
        super().__init__("org.openflexure.illumination", version="0.0.1")
        self._pwm1 = 0
        self._pwm2 = 0
        self._cc = 0
        def gui_func():
            forms = [
                {
                    "name": "Move stepper",
                    "route": "/illumination-set",
                    "isTask": True,
                    "isCollapsible": True,
                    "submitLabel": "Set Illumination",
                    "schema": [
                        {
                            "fieldType": "numberInput",
                            "name": "pwm1",
                            "label": "PWM Channel 1 %",
                            "value": self._pwm1,
                        },
                        {
                            "fieldType": "numberInput",
                            "name": "pwm2",
                            "label": "PWM Channel 2 %",
                            "value": self._pwm2,
                        },
                        {
                            "fieldType": "numberInput",
                            "name": "cc",
                            "label": "Constant Current %",
                            "value": self._cc,
                        }
                    ],
                }
            ]

            return {"icon": "tungsten", "forms": forms}

        self.add_view(SetIlluminationView, "/illumination-set")
        self.add_meta("gui", build_gui(gui_func, self))

    def set(self, action, pwm1, pwm2, cc):
        microscope = find_component("org.openflexure.microscope")
        sangaboard: Sangaboard  = microscope.stage.board
        self._pwm1 = pwm1
        self._pwm2 = pwm2
        self._cc = cc
        response = sangaboard.query(f"led_pwm 0 {pwm1/100}") #TODO: implement this properly
        if response != "Illumination set":
            action.data["result"] = "fail"
