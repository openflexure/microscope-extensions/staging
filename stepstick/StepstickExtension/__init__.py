from labthings import current_action, fields, find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.views import ActionView, PropertyView, View
from openflexure_microscope.api.utilities.gui import build_gui
from sangaboard import Sangaboard

class MoveActionView(ActionView):
    args = {
        "speed": fields.Int(required=True),
        "steps": fields.Int(required=True),
    }

    def post(self, args):
        extension = find_extension("org.openflexure.stepstick")
        action = current_action()

        return extension.move(args["speed"], args["steps"], action)

class ReleaseActionView(ActionView):
    def post(self):
        extension = find_extension("org.openflexure.stepstick")
        return extension.release(current_action()) #API only?


class StopActionView(ActionView):
    def post(self):
        extension = find_extension("org.openflexure.stepstick")
        return extension.stop(current_action())

# Create the extension class
class StepstickExtension(BaseExtension):
    def __init__(self):
        super().__init__("org.openflexure.stepstick", version="0.0.1")

        def gui_func():
            forms = [
                {
                    "name": "Move stepper",
                    "route": "/stepstick-move",
                    "isTask": True,
                    "isCollapsible": True,
                    "submitLabel": "Move",
                    "schema": [
                        {
                            "fieldType": "numberInput",
                            "name": "speed",
                            "label": "Speed",
                            "value": "20",
                        },
                        {
                            "fieldType": "numberInput",
                            "name": "steps",
                            "label": "steps",
                            "value": "200",
                        }
                    ],
                },
                {
                    "name": "Stop stepper",
                    "route": "/stepstick-stop",
                    "isTask": True,
                    "isCollapsible": True,
                    "submitLabel": "Stop",
                    "schema": [],
                },
                {
                    "name": "Release stepper",
                    "route": "/stepstick-release",
                    "isTask": True,
                    "isCollapsible": True,
                    "submitLabel": "Stop",
                    "schema": [],
                }
            ]
            
            return {"icon": "cyclone", "forms": forms}

        self.add_view(MoveActionView, "/stepstick-move")
        self.add_view(ReleaseActionView, "/stepstick-release")
        self.add_view(StopActionView, "/stepstick-stop")
        self.add_meta("gui", build_gui(gui_func, self))

    def move(self, speed, steps, action):
        microscope = find_component("org.openflexure.microscope")
        sangaboard: Sangaboard  = microscope.stage.board

        response = sangaboard.query(f"stepstick_move {speed} {steps}")
        if response != "Stepstick started":
            action.data["result"] = "fail"

    def release(self, action):
        microscope = find_component("org.openflexure.microscope")
        sangaboard: Sangaboard  = microscope.stage.board

        response = sangaboard.query(f"stepstick_release")
        if response != "Stepstick released":
            action.data["result"] = "fail"

    def stop(self, action):
        microscope = find_component("org.openflexure.microscope")
        sangaboard: Sangaboard  = microscope.stage.board

        response = sangaboard.query(f"stepstick_stop")
        if response != "Stepstick stopping":
            action.data["result"] = "fail"
